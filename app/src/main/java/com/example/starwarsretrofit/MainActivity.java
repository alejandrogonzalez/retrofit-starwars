package com.example.starwarsretrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.icu.text.UnicodeSetSpanner;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.starwarsretrofit.adapter.PersonajeAdapter;
import com.example.starwarsretrofit.model.Data;
import com.example.starwarsretrofit.model.Personaje;
import com.example.starwarsretrofit.webservice.WebServiceClient;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private PersonajeAdapter adapter;

    private List<Personaje> personajes;


    private Retrofit retrofit;
    private HttpLoggingInterceptor loggingInterceptor;
    private OkHttpClient.Builder httpClientBuilder;

    private Button buttonPrevious;
    private Button buttonNext;

    private ImageButton imageButton;

    private EditText searchBar;

    private Response<Data> respuesta;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recycler = findViewById(R.id.recyclerView);
        personajes = new ArrayList<Personaje>();

        adapter = new PersonajeAdapter(personajes, new PersonajeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Toast.makeText(MainActivity.this,"HOLA",Toast.LENGTH_LONG).show();
            }

        });


        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        lanzarPeticion("people/");

        imageButton = findViewById(R.id.imageButton);
        searchBar = findViewById(R.id.searchBar);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String peticionUsuario = "";

                Toast.makeText(MainActivity.this,searchBar.getText().toString(), Toast.LENGTH_LONG).show();
                if (searchBar.getText().toString() !=""){

                    peticionUsuario = searchBar.getText().toString();
                    lanzarPeticion("people/?search="+peticionUsuario);
                }else{
                    Toast.makeText(MainActivity.this,"No has introducido nada en la barra de busqueda",Toast.LENGTH_LONG).show();

                }

            }


        });

        buttonNext = findViewById(R.id.buttonNext);
        buttonPrevious = findViewById(R.id.buttonPrevious);

        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("hola");
                String[] urlCompleta = respuesta.body().getPrevious().split("/");
                String url = urlCompleta[urlCompleta.length - 1];

                Toast.makeText(MainActivity.this,"hola",Toast.LENGTH_LONG).show();
                lanzarPeticion("people/"+url);

            }

        });

      buttonNext.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              String[] urlCompleta = respuesta.body().getNext().split("/");
              String url = urlCompleta[urlCompleta.length - 1];
              lanzarPeticion("people/"+url);
          }
      });



    }

    private void lanzarPeticion(String url) {
        loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder().baseUrl("https://swapi.dev/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();

        WebServiceClient client = retrofit.create(WebServiceClient.class);

        Call<Data> call = client.getPersonajes(url);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                adapter.setPersonajes(response.body().getResults());

                respuesta = response;

            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
                Log.d("TAG1","Error: " + t.getMessage());

            }
        });


    }
}