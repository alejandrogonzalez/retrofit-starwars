package com.example.starwarsretrofit.webservice;

import com.example.starwarsretrofit.model.Data;
import com.example.starwarsretrofit.model.Personaje;

import java.util.concurrent.CopyOnWriteArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface WebServiceClient {

    @GET("people")
    Call<Data> getPersonajes();

    @GET()
    Call<Data> getPersonajes(@Url String url);

    @GET()
    Call<Personaje> getPersonaje(@Url String url);
}
