package com.example.starwarsretrofit.model;


import com.google.gson.annotations.SerializedName;

public class Personaje {

    private String name;
    private String height;
    @SerializedName("eye_color")
    private String eyeColor;
    @SerializedName("birth_year")
    private String birthday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthYear(String birtday) {
        this.birthday = birthday;
    }
}
