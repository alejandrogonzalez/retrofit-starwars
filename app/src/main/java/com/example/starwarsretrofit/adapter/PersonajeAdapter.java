package com.example.starwarsretrofit.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.starwarsretrofit.R;
import com.example.starwarsretrofit.model.Personaje;

import org.w3c.dom.Text;

import java.util.List;

public class PersonajeAdapter extends RecyclerView.Adapter<PersonajeAdapter.PersonajeAdapterHolder> {
    private List<Personaje> personajes;
    private OnItemClickListener itemClickListener;

    public PersonajeAdapter(List<Personaje>personajes, OnItemClickListener itemClickListener){
        this.personajes = personajes;
        this.itemClickListener = itemClickListener;
    }

    public void setPersonajes(List<Personaje> personajes) {
        this.personajes = personajes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PersonajeAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view,parent,false);
        return new PersonajeAdapterHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonajeAdapter.PersonajeAdapterHolder holder, int position) {
        holder.textViewName.setText(personajes.get(position).getName());
        holder.textViewHeight.setText(personajes.get(position).getHeight());
        holder.textViewEyeColor.setText(personajes.get(position).getEyeColor());
        holder.textViewBirthday.setText(personajes.get(position).getBirthday());
        holder.bind(this.itemClickListener);
    }

    @Override
    public int getItemCount() {
         return personajes.size();
    }

    public class PersonajeAdapterHolder extends RecyclerView.ViewHolder{
        TextView textViewName;
        TextView textViewHeight;
        TextView textViewEyeColor;
        TextView textViewBirthday;

        public PersonajeAdapterHolder(@NonNull View itemView){
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewHeight = itemView.findViewById(R.id.textViewHeight);
            textViewEyeColor = itemView.findViewById(R.id.textViewEyeColor);
            textViewBirthday = itemView.findViewById(R.id.textViewBirthday);
        }
        public void bind(final OnItemClickListener clickListener){
            itemView.setOnClickListener(v -> clickListener.onItemClick(getAdapterPosition()));
        }
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
