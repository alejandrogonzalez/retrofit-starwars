package com.example.starwarsretrofit;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.starwarsretrofit.model.Pelicula;
import com.example.starwarsretrofit.webservice.WebServiceClient;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DescripcionPelicula extends AppCompatActivity {

    private Retrofit retrofit;
    private HttpLoggingInterceptor loggingInterceptor;
    private OkHttpClient.Builder httpClientBuilder;

    private TextView textViewMovieTitle;

    private TextView textViewMovieDescription;

    private Button buttonMoviePrevious;

    private Button buttonMovieNext;

    private List<Pelicula> peliculas;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.item_view);


        buttonMovieNext = findViewById(R.id.buttonMovieNext);
        buttonMoviePrevious = findViewById(R.id.buttonPrevious);


    }


    private void lanzarPeticion(String url){
        loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder().baseUrl("https://swapi.dev/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();

        WebServiceClient client = retrofit.create(WebServiceClient.class);


    }


}
